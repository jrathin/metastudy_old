# JOGL-OpenCovid-metastudy
Metastudy of the OpenCovid community organisation and response on JOGL.

As an onboarding process, we suggest reading the [onboarding file](https://docs.google.com/document/d/15YNWmteB_H77Q30zV1KvuyRv1347XHAx46ebNU3-_Ak/edit#). 
More details for contributions are given below.


## Goals:

* **Describe and visualise the OpenCOVID19 community**

  * Analytics from the project 118 survey
  * Design simple questionnaires for the community to better understand the “user experience” towards impact.
  * Analytics from JOGL API (skills, projects) and Slack API (communications)
  * Skill map, communication network

* **Understand community organisation**

  * Network analyses of the evolution of the community: 
    * formation of subgroups
    * emerging leadership (number of contributions?)
    * stabilisation of activity
    * linkers between channels (brokers)

* **Help the community improve its efficiency**

  * Design of dashboard for representing the analytics and monitoring the community 
  * Design of recommendation systems to target needs to resources (i.e. notify people with the right skill set for a given need)
  * Onboarding of newcomers: find “godparents” with similar skills than newcomers who have been active in the community already and can help the onboarding process 


## Needs:

* Questionnaire design: UX designers, social scientists
* Digital data extraction: Slack and JOGL API 
* Dashboard: bokeh / react / Rshiny dev
* recommendation system: Algolia expert


## Contributing guidelines

**We use GitLab issues as a way to handle this project management. Issues 
can be found [here](https://gitlab.com/JOGL/metastudy/-/issues).**

Please let me know your gitlab handle (or create one) so I can add you as guests 
to use these issues as points of discussion and project management. 
The forum functionality is very practical and since we begin to have different 
subgroups forming and several transversal needs emerging, they will prove more 
and more needed.

You will also be able to add new issues as you have new subtasks that emerge. 
Please use the `metastudy` label when creating them, along with any other label 
that makes sense. These labels allow to quickly see where a newcomer can help. 
Assign the new task to yourself along with any other person involved.

For working on the code, you can just fork the metastudy repo, work on your end 
and then push back. We will just use the main repo issues as the place for 
centralizing info.

Do not hesitate to let me know (@msantolini or Marc Santolini on the Slack) 
if you need more guidance on this!
