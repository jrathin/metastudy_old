---
title: "R Notebook"
output: html_notebook
---

# Load data

```{r}
df <- read.csv('../create-dataframe/df_channels.csv')
```

```{r}
head(df)
```


```{r, fig.width=3,fig.asp=1}
plot.0(as.Date(df$created), 1:nrow(df), type='o', lwd=3, 
       xlab='',
       cex=0.1,ylab='Number of channels')
```


