---
title: "R Notebook"
output: html_notebook
---

```{r}
t <- read.delim('projects_jogl.tsv')
```

```{r}
dfs <- lapply(1:nrow(t), function(i){
  project <- t$short_title[i]
  
  user_list <- as.character(t$Users_num[i])
  users <- strsplit(user_list, ',')[[1]]
  
  data.frame('Project'=rep(project, length(users)), 'User'=users)

})

df_fin <- do.call(rbind, dfs)
```


```{r}
write.delim(df_fin, file='df_project_user.tsv')
```

