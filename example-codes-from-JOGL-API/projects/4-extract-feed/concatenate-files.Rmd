---
title: "R Notebook"
output: html_notebook
---
```{r}
files <- list.files('feeds/', pattern = '.tsv')
```

```{r}
ids <- sapply(files, function(f) strsplit(f, '\\.')[[1]][1])
```

```{r}
tf <- read.delim('feeds.tsv')
```

```{r}
tu <- read.delim('../../users/1-extract-data/users_jogl.tsv')
```


```{r}
df <- NULL
for (i in 1:length(files)){
  f <- files[i]
  id <- as.numeric(ids[i])
  t <- read.delim(paste0('feeds/',f))
  
  proj_name <- tf$nom[match(id, tf$feed_id)]
  user_name <- paste(tu$first_name, tu$last_name)[match(t$Id, tu$id)]
  df <- rbind(df, cbind(user_name, 
                        as.character(t$Type), 
                        rep(as.character(proj_name), nrow(t))))
}

colnames(df) <- c('UserId', 'Type','Project')
rownames(df) <- 1:nrow(df)
```


```{r}
write.delim(df, 'df_feeds.tsv')
```

