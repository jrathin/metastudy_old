---
title: "R Notebook"
output: html_notebook
---
```{r}
files <- list.files('followers/', pattern = '.tsv')
```

```{r}
ids <- sapply(files, function(f) strsplit(f, '\\.')[[1]][1])
```

```{r}
df <- NULL
for (i in 1:length(files)){
  f <- files[i]
  id <- as.numeric(ids[i])
  
  t <- read.delim(paste0('followers/',f))
  df <- rbind(df, cbind(as.numeric(t$Followers), rep(id, nrow(t))))
}

colnames(df) <- c('Source', 'Target')
rownames(df) <- 1:nrow(df)
```


```{r}
write.delim(df, 'df_followers.tsv')
```

