#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

import os
import sys
import pandas as pd

path = 'followers'

# r=root, d=directories, f = files
for r, d, f in os.walk(path):
    for file in f:
        if '.json' in file:
            file_f = os.path.join(r, file)
            table = pd.read_json(file_f, encoding='utf8')  

            users = [each['id'] for each in table['followers']]
            users = pd.DataFrame({'Followers':users})

            users.to_csv(file_f+'.tsv', sep='\t', index=False)
